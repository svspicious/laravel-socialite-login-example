@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Hello, {{Auth::user()->name}}.</p>
                    <div>email : {{Auth::user()->email}}</div>
                    @if (Auth::user()->provider)
                        <div>provider : {{Auth::user()->provider}}</div>
                    @endif
                    @if (Auth::user()->mobile)
                        <div>mobile : {{Auth::user()->mobile}}</div>
                    @endif
                    @if (Auth::user()->address)
                        <div>address : {{Auth::user()->address}}</div>
                    @endif
                    <div>created_at : {{Auth::user()->created_at}}</div>
                    <hr/>
                    <p>here goes some nerd stuff</p>
                    <pre>
                        {{var_dump(Auth::user())}}
                    </pre>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
