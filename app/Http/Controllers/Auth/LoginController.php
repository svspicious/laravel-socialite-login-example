<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // funtion to redirect to google login services
    function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }

    // callback login
    function handleGoogleCallback(){
        $user = Socialite::driver('google')->user();
        $authUser = User::where('email', $user->email)->first();
        // Check if user exist then login else create new user
        if(!$authUser){
            $authUser = User::create([
                'name'     => $user->name,
                'email'    => !empty($user->email)? $user->email : '' ,
                'provider' => "google",
                'provider_id' => $user->id
            ]);
        }

        #execute login
        Auth::login($authUser, true);
        return redirect('/home');
    }
}
